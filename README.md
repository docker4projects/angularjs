# Docker for Angular

<p align="center">
    <a href="https://angular.io/">
        <img src="https://angular.io/assets/images/logos/angular/angular.svg" alt="angular-logo" width="120px" height="120px"/>
    </a>
</p>

> This project is based on [Angular](https://angular.io/).

## Creating a new project

> If you have already created a project, go to [the next step](#launching-angular-application)

Use the Docker file generator to build a new project.

```bash
$ docker build -f Dockerfile.generator -t angular .
```

> If you use **Windows**, a `WARNING` can appear at the end of install. Don't forget to check and reset permissions for sensitive files and directories.

To run the container in your main directory:

```bash
// Linux
$ docker run -itd -v ${PWD}:/app --name angularWeb angular

// Windows
$ docker run -itd -v %cd%:/app --name angularWeb angular
```

Now you can use `ng` command without NodeJs in your machine.  
You can create a new angular project with this next command.  
**Before that, you need to rename or remove this `README.md` because the install is going to generate another one. If a README already exists, the install will fail.**

```bash
$ docker exec -it angularWeb ng new projectName --directory=.
```

You can remove `angularWeb` container as you want.

```bash
$ docker rm -f angularWeb
```

And you can drop `Dockerfile.generator`.

## Launching Angular application

Now, you just need the `Dockerfile` and the `docker-compose.yml` in your root project.

```bash
$ docker-compose up
```

## Development

You can create a component through docker-compose with this command line:

```bash
$ docker-compose exec web ng generate component my-component
```

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

```bash
$ docker-compose exec web ng test
```

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

```bash
$ docker-compose exec web ng e2e --port=4201
```

> The port depends on your parameters in `docker-compose.yml`

## Production

Run command to run production server. Navigate to [`http://localhost:4200/`](http://localhost:4200/).

```bash
$ docker-compose -f docker-compose.prod.yml up --build
```
